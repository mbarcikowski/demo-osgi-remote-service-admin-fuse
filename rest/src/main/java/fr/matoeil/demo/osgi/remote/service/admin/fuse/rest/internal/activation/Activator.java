/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.demo.osgi.remote.service.admin.fuse.rest.internal.activation;

import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import fr.matoeil.demo.osgi.remote.service.admin.fuse.api.EchoService;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.servlet.Servlet;
import java.util.Dictionary;
import java.util.Hashtable;

import static com.google.inject.Guice.createInjector;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Component
public class Activator {
    private static final Logger LOGGER = LoggerFactory.getLogger(Activator.class);

    private EchoService echoService_;

    private GuiceContainer guiceContainer_;
    private ServiceRegistration servletServiceRegistration_;

    @Activate
    public void start(BundleContext aBundleContext) {
        LOGGER.debug("Activator.start ");
        createInjector(new BundleModule(echoService_)).injectMembers(this);

        Dictionary<String, Object> properties = new Hashtable<>();
        properties.put("alias", "/rest");
        properties.put("servlet-name", "echo rest servlet");
        properties.put("init.com.sun.jersey.api.json.POJOMappingFeature", Boolean.TRUE);
        servletServiceRegistration_ = aBundleContext.registerService(Servlet.class, guiceContainer_, properties);
    }


    @Deactivate
    public void stop() {
        LOGGER.debug("Activator.stop");
        servletServiceRegistration_.unregister();
    }

    @Reference
    public void bindEchoService(final EchoService aEchoService) {
        LOGGER.debug("Activator.bindEchoService");
        echoService_ = aEchoService;
    }

    public void unbindEchoService(final EchoService aEchoService) {
        LOGGER.debug("Activator.unbindEchoService");
        echoService_ = null;
    }

    @Inject
    public void setGuiceContainer(final GuiceContainer aGuiceContainer) {
        guiceContainer_ = aGuiceContainer;
    }
}
