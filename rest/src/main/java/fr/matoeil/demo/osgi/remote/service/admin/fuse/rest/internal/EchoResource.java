/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.demo.osgi.remote.service.admin.fuse.rest.internal;


import fr.matoeil.demo.osgi.remote.service.admin.fuse.api.EchoService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Path("/Echo")
public class EchoResource {
    private final EchoService echoService_;

    @Inject
    public EchoResource(final EchoService aEchoService) {
        echoService_ = aEchoService;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String doEcho(@QueryParam("message") String aMessage) {
        return echoService_.doEcho(aMessage);
    }
}
