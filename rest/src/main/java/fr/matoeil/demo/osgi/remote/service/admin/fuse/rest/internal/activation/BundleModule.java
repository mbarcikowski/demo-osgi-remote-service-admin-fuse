/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.demo.osgi.remote.service.admin.fuse.rest.internal.activation;

import com.google.inject.AbstractModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import fr.matoeil.demo.osgi.remote.service.admin.fuse.api.EchoService;
import fr.matoeil.demo.osgi.remote.service.admin.fuse.rest.internal.EchoResource;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class BundleModule extends AbstractModule {

    private final EchoService echoService_;

    public BundleModule(final EchoService aEchoService) {
        echoService_ = aEchoService;
    }

    @Override
    protected void configure() {
        bind(EchoService.class).toInstance(echoService_);

        bind(JacksonJsonProvider.class).asEagerSingleton();
        bind(EchoResource.class).asEagerSingleton();
        bind(GuiceContainer.class).asEagerSingleton();

    }
}
